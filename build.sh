#!/usr/bin/env bash

set -e

function project_setup {
  PROJECT_GO_DIR=gitlab.com/telecom-tower

  PROJECT_BASE=$(pwd)
  PROJECT_NAME=$(basename ${PROJECT_BASE})

  mkdir -p ${GOPATH}/src/${PROJECT_GO_DIR}
  cd ${GOPATH}/src/${PROJECT_GO_DIR}
  ln -s ${PROJECT_BASE}

  cd ${PROJECT_NAME}

  go get -u github.com/mjibson/esc
}

function project_cleanup {
  rm -Rf ${GOPATH}/src/*
  rm -Rf ${GOPATH}/pkg/*
}


function project_build_arm {
  export GOOS=linux
  export GOARCH=arm
  cd apps/ttudp
  go generate
  go get -v -d ./...
  go build
}

function project_test {
  go get -u github.com/golang/lint/golint

  cd apps/ttudp
  go generate

  cd ${GOPATH}/src/${PROJECT_GO_DIR}/${PROJECT_NAME}
  go get -v -t -d ./...
  go test ./...
  go test -cover ./...
  go vet ./...

  cd database
  echo "----- golint database ----"
  ls *.go | grep -v '\.pb\.go$' | xargs golint
  cd ${GOPATH}/src/${PROJECT_GO_DIR}/${PROJECT_NAME}

  echo "----- golint apps/ttudp ----"
  cd apps/ttudp
  ls *.go | grep -v '\.pb\.go$' | xargs golint
  cd ${GOPATH}/src/${PROJECT_GO_DIR}/${PROJECT_NAME}
}


case "$1" in
  build)
    project_setup
    project_build_arm
    ;;

  test)
    project_setup
    project_test
    ;;

  clean)
    project_cleanup
    ;;

  *)
    echo $"Usage: $0 {build|test|clean}"
    exit 1

esac