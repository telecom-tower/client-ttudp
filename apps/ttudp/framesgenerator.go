package main

import (
	"math/rand"

	log "github.com/sirupsen/logrus"
	"gitlab.com/geomyidae/ws2811client"
	"gitlab.com/telecom-tower/font"
	"gitlab.com/telecom-tower/pixtrix"
)

const (
	ballProbability = 0.02
	maxNoBall       = 60
)

/* ****************************************************************
 * frame generator
 * ****************************************************************/

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func framesGenerator(tt *ws2811client.Ws2811Client, channel chan interface{}) { // nolint: gocyclo

	// create cache
	frameNr := make(chan int, framesBufferLen-1)
	frames := make([]*pixtrix.Pixtrix, framesBufferLen)

	for i := range frames {
		frames[i] = &pixtrix.Pixtrix{
			Rows:   rows,
			Bitmap: make([]uint32, rows*columns),
		}
	}

	// create balls
	balls := make([]*ball, 0)

	// generate the frames
	go func() {
		i := 0
		cnt := 0
		rolling := 0
		advanceRolling := 0
		isBlack := false
		message := playersMessage{}
		message.MsgType = msgPlayers
		message.channel = make(chan map[string]interface{})

		var answer map[string]interface{}
		var players []*player

		rollingText := pixtrix.NewMatrix(rows, 0)
		w := pixtrix.NewWriter(rollingText)
		w.Spacer(columns, 0x000000)
		msg := "Joue avec moi - Spiel mit mir - Gioca con me - Play with me - "
		w.WriteText(msg, font.Font6x8, 0x996600, 0x000000)
		rollingText.Append(rollingText.Slice(columns, 2*columns))
		noBallCount := 0

		for {
			prevBalls := balls
			balls = make([]*ball, 0)
			for _, ball := range prevBalls {
				if ball.col >= 0 && ball.col < columns+trailLength {
					balls = append(balls, ball)
				}
			}
			if rand.Float32() < ballProbability || noBallCount > maxNoBall {
				balls = append(balls, newBall())
				noBallCount = 0
			} else {
				noBallCount++
			}

			frame := frames[i]
			channel <- &message
			answer = <-message.channel

			players = answer["players"].([]*player)
			message.ID = -1 // reset !

			if len(players) == 0 {
				// Rolling message
				for x := 0; x < frame.Columns(); x++ {
					for y := 0; y < frame.Rows; y++ {
						frame.SetPixel(x, y, rollingText.Slice(rolling, rolling+columns).GetPixel(x, y))
					}
				}
				if advanceRolling == 0 {
					rolling++
				}
				advanceRolling = (advanceRolling + 1) % 2

				if rolling+columns >= rollingText.Columns() {
					rolling = columns
				}
			} else {
				rolling = 0
				advanceRolling = 0
				// all black
				for x := 0; x < frame.Columns(); x++ {
					for y := 0; y < frame.Rows; y++ {
						frame.SetPixel(x, y, 0x000000)
					}
				}
			}

			// baballe
			for _, ball := range balls {
				ball.draw(frame)
			}

			for _, p := range players {

				switch p.state {

				case statePlaying:
					start, stop := p.paddle()

					for y := start; y < stop; y++ {
						c := frame.GetPixel(p.id, y)
						if c == catchBallColor || c == avoidBallColor {
							message.ID = p.id
							message.ballColor = c
							p.touched = touchedAnimationFrames
						}
						frame.SetPixel(p.id, y, p.color)
					}
					if p.touched > 0 {
						touchedColor := p.color
						if p.touched%2 == 0 {
							touchedColor = 0xFFFFFF
						}
						for y := start; y < stop; y++ {
							frame.SetPixel(p.id+1, y, touchedColor)
							frame.SetPixel(p.id, y, touchedColor)
							frame.SetPixel(p.id-1, y, touchedColor)
						}
						p.touched--
					}

				case stateInit:
					color := uint32(0)
					if !isBlack {
						color = p.color
					}
					for y := frame.Rows - 1; y >= 0; y-- {
						frame.SetPixel(p.id, y, color)
					}
				}

			}
			frameNr <- i
			i = (i + 1) % framesBufferLen

			cnt++
			if cnt > initFlashCountOn {
				isBlack = true
			} else {
				isBlack = false
			}
			if cnt > initFlashCountOn+initFlashCountOff {
				cnt = 0
			}
		}
	}()

	// send new frames to the tower
	go func() {
		for {
			i := <-frameNr
			err := tt.Update(frames[i].InterleavedStripes()[0])
			if err != nil {
				log.Fatal(err)
			}
			//time.Sleep(50 * time.Millisecond)
		}
	}()

}
