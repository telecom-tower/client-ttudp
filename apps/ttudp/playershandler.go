package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

/*****************************************************************
 * ws handling
 *****************************************************************/

func handlePlayerWs(conn *websocket.Conn, positionCookie *http.Cookie, toEvtProcessChan chan interface{}) { // nolint: gocyclo

	fromEvtProcessChan := make(chan map[string]interface{})
	id := -1

	for {
		// read message from ws
		_, bs, err := conn.ReadMessage() // type is websocket.TextMessage
		if err != nil {
			toEvtProcessChan <- &SimpleMessage{MsgType: msgClose, ID: id}
			close(fromEvtProcessChan)
			break
		}

		// get message type
		rawMessage := SimpleMessage{}
		err = json.Unmarshal(bs, &rawMessage)
		if err != nil {
			log.Errorf("got an invalid message: %s\n", string(bs))
			continue
		}

		// find out the final message
		var msg interface{}
		switch rawMessage.MsgType {
		case msgInit:
			initMsg := InitMessage{}
			if err = json.Unmarshal(bs, &initMsg); err != nil {
				continue
			}
			initMsg.ID = id
			initMsg.channel = fromEvtProcessChan
			initMsg.conn = conn
			// getting cookie if exists
			if positionCookie != nil {
				// TODO extract scanf
				var key string
				var pos int
				if n, err1 := fmt.Sscanf(positionCookie.Value, "%d:%s", &pos, &key); n == 2 && err1 == nil && ttDB.IsBookingValid(int32(pos), key) {
					initMsg.Position = pos
				}
			}
			msg = &initMsg

		case msgMvmt:
			mvmtMsg := MvmtMessage{}
			if err = json.Unmarshal(bs, &mvmtMsg); err != nil {
				continue
			}
			mvmtMsg.ID = id
			mvmtMsg.channel = fromEvtProcessChan
			msg = &mvmtMsg

		default:
			rawMessage.ID = id
			rawMessage.channel = fromEvtProcessChan
			msg = &rawMessage
		}

		// send message to events process loop
		toEvtProcessChan <- msg
		// get answer from process loop
		result := <-fromEvtProcessChan
		if msg.(MessageInterface).getType() == msgInit {
			id = result["id"].(int)
		}

	}

}
