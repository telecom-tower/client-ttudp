package main

import (
	"github.com/gorilla/websocket"
)

const (
	// type of a ScoreMessage (Server -> Client)
	msgScore = "p"
	// type of an InitMessage (Client -> Server)
	msgInit = "i"
	// type of a SimpleMessage (Client -> Server) asking to start the game. The server will answer with a ScoreMessage.
	msgStart = "s"
	// type of a MvmtMessage (Client -> Server) asking to move the bar.
	msgMvmt = "m"
	// type of a SimpleMessage (Server -> Client) to notify a game over.
	msgGameover = "x"
	// type of an internal playersMessage (Frame Generator -> Event Processor)
	// to notify a player's bar has touched a ball
	msgPlayers = "players"
	// type of a SimpleMessage (Players Handler -> Event Processor) sent if an error occurred while reading the
	// websocker. The processor will then delete the player.
	msgClose = "close"
)

// MessageInterface defines common methods for every message passed between HTTP client and server.
// A message always has a type, the id of the player and a way to get the channel associated to it
// (used to send response)
type MessageInterface interface {
	// getType return the type of the message, i.e. a letter defining the specific message.
	// see the const above
	getType() string
	// getID returns the id of the player associated to the message
	getID() int
	// getChannel returns the channel used to synchronise the players handler and the events process loop.
	// it is instantiated by the players handler and passed to the processor within each message. The players
	// handler will then wait for a signal continue processing player messages. So in short, it ensures that
	// only one message from the same player is processed at a time.
	getChannel() chan map[string]interface{}
}

// SimpleMessage is used for any message without a specific payload
type SimpleMessage struct {
	// a letter defining the type of the message
	MsgType string `json:"type"`
	// the id of the player
	ID int `json:"id"`
	// the channel used to send an answer
	channel chan map[string]interface{}
}

func (m *SimpleMessage) getType() string {
	return m.MsgType
}

func (m *SimpleMessage) getID() int {
	return m.ID
}

func (m *SimpleMessage) getChannel() chan map[string]interface{} {
	return m.channel
}

// InitMessage is sent by the HTTP client to start a new game.
type InitMessage struct {
	SimpleMessage
	// The color to use for the bar
	Color uint32 `json:"color"`
	// The height of the bar, which will affect the difficulty of the game
	Height int `json:"height"`
	// The desired or reserved position of the bar on the tower.
	// Between 1 and columns (128) for a specific position,
	// set to 0 or less for a random one
	Position int `json:"position"`
	// Should be set only if the position has been reserved (see positions.go).
	// In this case, this key will ensure that the client is indeed the one who reserved the position.
	PositionKey string `json:"positionKey"`
	// WebSocket connected to the client in order to send the answer
	conn *websocket.Conn
}

// MvmtMessage asks the server to move the bar during play (see Mvmt)
// or to move vertically at a specific position (Pos).
type MvmtMessage struct {
	SimpleMessage
	// Set to +1 for up, -1 for down.
	Mvmt int `json:"direction"`
	// Vertical position of the bar's top pixel. It must be between inputPositionMin and inputPositionMax
	// (-5 and 5). The exact position will be computed using a translation (see eventsprocessor.go)
	Pos float64 `json:"position"`
}

// ScoreMessage is sent to the HTTP client every time the score changes, so when the player touched a ball.
type ScoreMessage struct {
	SimpleMessage
	// the current score
	Score
}

// playersMessage is used internally by the frames generator (see framesgenerator.go) to inform the
// event processor that the player touched a ball. The processor will then update the score and send a
// ScoreMessage (or a gameover message) to the HTTP client to notify the change.
type playersMessage struct {
	SimpleMessage
	// The color of the ball touched, used to determine if the score should be increased or decreased.
	ballColor uint32
}
