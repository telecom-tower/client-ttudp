package main

import (
	"bytes"
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/pkg/errors"
	"gitlab.com/telecom-tower/client-ttudp/database"
)

const (
	positionCookieName = "ttUDP_pos"
)

// -------------------------  HTTP handler

// PositionBookingHandler is the handler for the booking of a position
func PositionBookingHandler(w http.ResponseWriter, r *http.Request) error {
	force := r.FormValue("force")

	var err error
	var hours, position int
	var positionStr string

	if hours, err = strconv.Atoi(r.FormValue("hours")); err != nil {
		return fmt.Errorf("'hours' argument: expecting an integer %s", positionStr)
	}

	positionStr = r.FormValue("position")
	if position, err = strconv.Atoi(positionStr); err != nil {
		return fmt.Errorf("'position' argument: expecting an integer %s", positionStr)
	}

	if position < 1 || position >= columns {
		return fmt.Errorf("Wrong position: %d", position)
	}

	if hours < 0 {
		return fmt.Errorf("Wrong hour: %d", hours)
	}

	// check cache for existing booking
	if database.IsBooked(int32(position)) && force == "" {
		// no force: don't override valid booking
		return errors.New("Position already taken")
	}

	pb := database.NewPositionValue(position, hours)
	if err = ttDB.AddBooking(pb); err != nil {
		err = errors.WithMessage(err, "failed to add booking")
		log.Error(err)
		return err
	}

	// set cookie
	cookieValue := fmt.Sprintf("%d:%s", position, *(pb.Key))
	http.SetCookie(w, &http.Cookie{Name: positionCookieName, Path: "/",
		Value: cookieValue, Expires: time.Unix(0, *(pb.Expire))})
	_, err = w.Write([]byte(cookieValue))
	if err != nil {
		log.Error(errors.WithMessage(err, "Can't write cookie"))
	}
	return err
}

// ClearBookingsHandler clears a booking
func ClearBookingsHandler(w http.ResponseWriter, r *http.Request) {
	positionStr := r.FormValue("position")
	position, err := strconv.Atoi(positionStr)

	if err != nil {
		http.Error(w, fmt.Sprintf("'position' argument: expecting an integer %s", positionStr), 500)
		return
	}

	err = ttDB.DeleteBooking(int32(position))
	if err != nil {
		err = errors.WithMessage(err, "Error deleting booking")
		log.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if cookie, _ := r.Cookie(positionCookieName); cookie != nil {
		cookie.MaxAge = -1
		cookie.Path = "/" // without a path, the browser will take the url by default -> won't modify the previous one
		http.SetCookie(w, cookie)
	}

	w.Write([]byte("Position released... And cookie deleted.")) // nolint: errcheck
}

// ListBookingsHandler shows all bookings
func ListBookingsHandler(w http.ResponseWriter, r *http.Request) {
	// use a buffer because I want the "current active key" first,
	// but the length of the BookedPositions might be modified during
	// the loop ... :)
	buff := new(bytes.Buffer)
	for _, pv := range database.BookedPositions {
		// this call will clear the entry if it is expired
		if ttDB.IsBookingValid(*(pv.Position), *(pv.Key)) {
			buff.WriteString("    " + pv.NiceString() + "\n")
		}
	}
	w.Write([]byte("Current active keys: "))                              // nolint: errcheck
	w.Write([]byte(fmt.Sprintf("(%d)\n", len(database.BookedPositions)))) // nolint: errcheck
	w.Write(buff.Bytes())                                                 // nolint: errcheck
}
