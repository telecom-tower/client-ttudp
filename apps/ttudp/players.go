package main

import (
	//log "github.com/sirupsen/logrus"
	"github.com/gorilla/websocket"
)

// player

// Score records the score of a player
type Score struct {
	Lives  byte   `json:"lives"`
	Points uint32 `json:"points"`
}

type initAnim struct {
	color uint32
}

type player struct {
	id      int // also the column
	state   int // state (see const P_)
	height  int // depends on the difficulty
	pos     int // position of the top pixel
	touched int // for the animation when ball caught
	color   uint32
	init    initAnim // used for init animation
	conn    *websocket.Conn
	score   Score
}

func (p *player) paddle() (start, stop int) {
	start = p.pos
	if start < 0 {
		start = 0
	}
	stop = p.pos + p.height
	if stop > rows {
		stop = rows
	}
	return start, stop
}

// playerlist

type playerList map[int]*player

func (players playerList) cloneToArray() []*player {
	ps := make([]*player, 0)
	for _, v := range players {
		clone := *v
		ps = append(ps, &clone)
	}
	return ps
}
