package main

import (
	"math/rand"

	"gitlab.com/telecom-tower/pixtrix"
)

const (
	catchBallProbability = 0.6
	catchBallColor       = 0xFFDF00
	avoidBallColor       = 0xFF0000
	trailLength          = 4
)

// ------------------------------------ ball

type ball struct {
	row, col          int
	direction         int
	speed             int
	angle             int
	frameBetweenMoves int
	color             uint32
	nextRowChange     int
	trail             []int
}

func newBall() *ball {
	ball := new(ball)
	ball.direction = -1 + 2*rand.Intn(2)
	if ball.direction > 0 {
		ball.col = 0
	} else {
		ball.col = columns - 1
	}
	ball.row = int(rand.Int31n(int32(rows)))
	ball.frameBetweenMoves = ball.speed
	ball.angle = int(-7 + rand.Int31n(15))
	if rand.Float32() < catchBallProbability {
		ball.color = catchBallColor
	} else {
		ball.color = avoidBallColor
	}
	ball.nextRowChange = abs(ball.angle)
	ball.trail = make([]int, trailLength)
	return ball
}

func (ball *ball) move() {

	ball.frameBetweenMoves--

	// time to move
	if ball.frameBetweenMoves <= 0 {
		// add old column to trail
		ball.addTrail(ball.row)
		// change column
		ball.col += ball.direction
		ball.frameBetweenMoves = ball.speed

		// depending on the "angle", change row
		// after some time
		if ball.nextRowChange <= 0 {
			// if a "wall" is reached, inverse "angle"
			if ball.angle < 0 {
				ball.row++
			} else if ball.angle > 0 {
				ball.row--
			}
			// compute time before next row change
			ball.nextRowChange = abs(ball.angle)
		}

		if ball.row <= 0 {
			ball.angle = -abs(ball.angle)
		} else if ball.row >= rows-1 {
			ball.angle = abs(ball.angle)
		}
		ball.nextRowChange--
	}
}

func (ball *ball) addTrail(row int) {
	// add new
	ball.trail = append(ball.trail, row)
	// pop older ones
	if len(ball.trail) > trailLength {
		ball.trail = ball.trail[len(ball.trail)-trailLength:]
	}
}

func (ball *ball) draw(frame *pixtrix.Pixtrix) {
	if ball.col >= 0 && ball.col < columns {
		frame.SetPixel(ball.col, ball.row, ball.color)
	}

	col := ball.col
	mask := uint32(0xFF7F7F7F)
	color := ball.color

	for i := len(ball.trail) - 1; i >= 0; i-- {
		color = (color >> 1) & mask
		col -= ball.direction // direction is -1 or +1
		if col < columns && col >= 0 {
			frame.SetPixel(col, ball.trail[i], color)
		}
	}

	ball.move()

}
