package main

import (
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/telecom-tower/client-ttudp/database"
)

const (
	inputPositionMin = -5
	inputPositionMax = 5
	hidingMargin     = 2
)

/* *****************************************************************
 * event process loop
 * ****************************************************************/

func processEvents(messageChan chan interface{}) { // nolint: gocyclo
	players := playerList{}

	for {
		raw, ok := <-messageChan
		msg := raw.(MessageInterface)
		if !ok {
			break
		}

		switch msg.getType() {

		case msgInit:
			log.Debug("Received init message")
			initMsg := msg.(*InitMessage)
			var np *player
			np = &player{
				id:     -1,
				state:  stateInit,
				color:  initMsg.Color,
				height: initMsg.Height,
				init:   initAnim{color: initMsg.Color},
				conn:   initMsg.conn,
			}

			if initMsg.Position > 0 && initMsg.Position < columns && players[initMsg.Position] == nil {
				// check for preferred position
				np.id = initMsg.Position
			} else {
				// find the next available position
				for _, i := range playersPositions {
					if players[i] == nil && !database.IsBooked(int32(i)) {
						np.id = i
						break
					}
				}
			}

			if np.id > 0 {
				log.Debugf("Player id: %v", np.id)
				players[np.id] = np

				ans := SimpleMessage{}
				ans.MsgType = msgInit
				ans.ID = np.id

				err := initMsg.conn.WriteJSON(&ans)
				if err != nil {
					log.Error(errors.WithMessage(err, "Error writing JSON"))
				} else {
					initMsg.channel <- map[string]interface{}{"id": np.id}
				}

			} else {
				log.Errorf("No position left.., %v\n", initMsg) // TODO
				initMsg.channel <- map[string]interface{}{"id": -1}
			}

		case msgStart:
			log.Debug("Received start message")
			startMsg := msg.(*SimpleMessage)
			if p := players[startMsg.ID]; p != nil {
				p.state = statePlaying
				p.pos = middlePos - (p.height / 2)
				p.score = Score{defaultLives, 0}
				answer := ScoreMessage{*startMsg, p.score}
				p.conn.WriteJSON(answer) // nolint: errcheck
			}
			startMsg.channel <- nil

		case msgMvmt:
			log.Debug("Received mvmt message")
			mvmtMessage := msg.(*MvmtMessage)
			if p := players[mvmtMessage.ID]; p != nil {
				mvmt := mvmtMessage.Mvmt
				if mvmt == 1 {
					// down
					if p.pos < rows-hidingMargin {
						p.pos++
					}
				} else if mvmt == -1 {
					//up
					if p.pos > hidingMargin-p.height {
						p.pos--
					}
				} else {
					// translate to get the new player position
					outMin := float64(hidingMargin - p.height)
					outMax := float64(rows - hidingMargin)
					p.pos = int((mvmtMessage.Pos-inputPositionMin)/(inputPositionMax-inputPositionMin)*
						(outMax-outMin) + outMin)
				}
				p.conn.WriteJSON(mvmtMessage) // nolint: errcheck
				mvmtMessage.channel <- nil
			}

		case msgClose:
			log.Debug("Received close message")
			delete(players, msg.getID())
			log.Debugln("Closing token")

		case msgPlayers:
			// log.Debug("Received players message")
			playersMsg := msg.(*playersMessage)
			if playersMsg.ID >= 0 {
				// something happened: ball caught
				if p := players[playersMsg.ID]; p != nil {
					// update score
					if playersMsg.ballColor == catchBallColor {
						p.score.Points += ballCaughtBonus
					} else {
						p.score.Lives--
					}

					if p.score.Lives <= 0 {
						// game over
						gameOverMsg := SimpleMessage{MsgType: msgGameover}
						p.conn.WriteJSON(gameOverMsg) // nolint: errcheck
					} else {
						// inform the interface of a score change
						scoreMsg := ScoreMessage{SimpleMessage{MsgType: msgScore}, p.score}
						p.conn.WriteJSON(scoreMsg) // nolint: errcheck
					}
				}
			}

			msg.getChannel() <- map[string]interface{}{"players": players.cloneToArray()}
		}

	}
}
