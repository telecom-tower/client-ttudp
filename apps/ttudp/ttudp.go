package main // import "gitlab.com/telecom-tower/client-ttudp/apps/ttudp"

//go:generate esc -o html.go -prefix ../../web ../../web

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/vharitonsky/iniflags"
	"gitlab.com/geomyidae/ws2811client"
	"gitlab.com/telecom-tower/client-ttudp/database"
)

const (
	framesBufferLen = 1
)

const (
	columns                = 128
	rows                   = 8
	defaultBrightness      = 64
	initFlashCountOn       = 10
	initFlashCountOff      = 3
	defaultLives           = 3
	ballCaughtBonus        = 100
	middlePos              = 4
	touchedAnimationFrames = 6
)

var (
	playersPositions = []int{
		64, 32, 96, 16, 48, 80, 112, 8, 24, 40, 56, 72, 88, 104, 120,
	}
	ttDB *database.TTDB
)

const (
	stateInit    = iota
	statePlaying = iota
)

// Admin functions

//func checkLocal(r *http.Request) error {
//	ip, _, err := net.SplitHostPort(r.RemoteAddr)
//	if err != nil {
//		return errors.New("Invalid source IP")
//	}
//	if ip != "127.0.0.1" && ip != "::1" {
//		return errors.New(fmt.Sprintf("%v: Unauthorized", ip))
//	}
//	return nil
//}

func leaderBoard(w http.ResponseWriter, r *http.Request) {
	type Entry struct {
		Username string `json:"username"`
		Score    uint32 `json:"score"`
	}
	type Result struct {
		TopScore       []Entry `json:"top_score"`
		TopDailyScore  []Entry `json:"top_daily_score"`
		TopHourlyScore []Entry `json:"top_hourly_score"`
	}

	res := Result{}
	res.TopScore = make([]Entry, 0)
	res.TopDailyScore = make([]Entry, 0)
	res.TopHourlyScore = make([]Entry, 0)

	l, err := ttDB.TopScores()
	if err != nil {
		log.Errorf("Unable to get TopScores: %v", err)
		http.Error(w, fmt.Sprintf("Unable to get TopScores: %v", err), http.StatusInternalServerError)
		return
	}
	for _, v := range l {
		res.TopScore = append(res.TopScore, Entry{Username: *v.Username, Score: *v.Score})
	}

	l, err = ttDB.TopDailyScores(time.Now())
	if err != nil {
		http.Error(w, fmt.Sprintf("Unable to get TopDailyScores: %v", err), http.StatusInternalServerError)
		return
	}
	for _, v := range l {
		res.TopDailyScore = append(res.TopDailyScore, Entry{Username: *v.Username, Score: *v.Score})
	}

	l, err = ttDB.TopHourlyScores()
	if err != nil {
		http.Error(w, fmt.Sprintf("Unable to get TopHoulryScores: %v", err), http.StatusInternalServerError)
		return
	}
	for _, v := range l {
		res.TopHourlyScore = append(res.TopHourlyScore, Entry{Username: *v.Username, Score: *v.Score})
	}

	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(res); err != nil {
		http.Error(w, fmt.Sprintf("Failure to encode JSON: %v", err), http.StatusInternalServerError)
		return
	}

}

func saveScore(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		err = errors.WithMessage(err, "Error pardsing form")
		log.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	username := r.Form.Get("username")
	if username == "" {
		http.Error(w, "no username", http.StatusInternalServerError)
		return
	}
	score := r.Form.Get("score")
	if score == "" {
		http.Error(w, "no score", http.StatusInternalServerError)
		return
	}
	scoreN, err := strconv.ParseInt(score, 10, 32)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := ttDB.SaveScore(username, uint32(scoreN)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprintln(w, "OK") // nolint: gas
}

func cleanupDailyScores(w http.ResponseWriter, r *http.Request) {
	if err := ttDB.CleanupDailyScores(time.Now()); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprintln(w, "OK") // nolint: gas
}

/******************************************************************
 * main
 *****************************************************************/

func main() { // nolint: gocyclo
	// parse arguments
	var brightness = flag.Int(
		"brightness", defaultBrightness,
		"Brightness between 0 and 255.")
	var debug = flag.Bool("debug", false, "be verbose")
	var port = flag.Int("port", 80, "HTTP daemon port")
	iniflags.Parse()

	// set logging
	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debugln("Running in debug mode")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	var err error
	// open database
	ttDB, err = database.NewttDB()
	if err != nil {
		log.Fatalf("Unable to connect to database: %v", err)
		os.Exit(1)
	}
	if err = ttDB.Init(); err != nil {
		log.Fatalf("Unable to initializedatabase: %v", err)
		os.Exit(1)
	}

	// create the connexion to the telecom-tower server
	log.Infoln("Connection to the tower")
	tt, err := ws2811client.LocalClient("ws://127.0.0.1:8484/")
	if err != nil {
		log.Error(err)
		os.Exit(1)
	}
	tt.Brightness = *brightness
	if _, err := tt.Open(1000 * time.Millisecond); err != nil {
		log.Errorln(err)
		os.Exit(1)
	}

	// initalize wamp (websockets)
	log.Debugln("Opening ws...")

	// create the players
	messagesChan := make(chan interface{})

	// launch the routines
	go framesGenerator(tt, messagesChan)
	go func() {
		processEvents(messagesChan)
	}()

	// websocket
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	// setup http server
	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Infof("New game request from %v", r.RemoteAddr)
		src, err := FS(false).Open("/index.html")
		if err != nil {
			http.NotFound(w, r)
			return
		}
		http.ServeContent(w, r, "index.html", time.Time{}, src)
	})

	r.HandleFunc("/positions", func(w http.ResponseWriter, r *http.Request) {
		if err := PositionBookingHandler(w, r); err != nil {
			http.Error(w, err.Error(), 500)
		}
	})
	r.HandleFunc("/positions/clear", ClearBookingsHandler)
	r.HandleFunc("/positions/list", ListBookingsHandler)

	r.HandleFunc("/leaderboard", leaderBoard)
	r.HandleFunc("/save-score", saveScore)
	r.HandleFunc("/cleanup-daily-scores", cleanupDailyScores)

	r.Path("/ws").Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Debugln("New websocket request")
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Error(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		cookie, _ := r.Cookie(positionCookieName)
		log.Debugf("Handling player, cookie=%v", cookie)
		handlePlayerWs(conn, cookie, messagesChan)
	}))

	r.PathPrefix("/").Handler(http.FileServer(FS(false)))

	log.Infof("Starting server on port %v", *port)
	srv := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf(":%v", *port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
	log.Infoln("Main loop terminated!")
	tt.Close()
}
