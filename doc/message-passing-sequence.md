# Diagrams

Here are the flow of messages during a game lifecycle.

We use [js-sequence-diagrams](https://bramp.github.io/js-sequence-diagrams/) to render the diagrams. Here are the 
definitions.


__start game__

```diagram
Title: TTUDP Interaction diagram - start game
Client->Server: InitMessage
Server->Client: SimpleMessage(msg_init)
Client->Server: SimpleMessage(msg_start)
Server->Client: ScoreMessage
```

__play__

``` diagram
Title: TTUDP Interaction diagram - play 
Client->Server: MvmtMessage
Server->Client: MvmtMessage
Client->Server: MvmtMessage
Server->Client: MvmtMessage
FrameGenerator-->Server: playersMessage
Server->Client: ScoreMessage
```

__end game__

```diagram
Title: TTUDP Interaction diagram - end game 
participant FrameGenerator
participant Server
participant PlayerHandler

FrameGenerator-->Server: playersMessage
Server->Client: SimpleMessage(msg_gameover)
Client->PlayerHandler: close ws
PlayerHandler-->Server: SimpleMessage(msg_close)
```

<!-- scripts to render the diagrams -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.5.1/snap.svg-min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/js-sequence-diagrams/1.0.6/sequence-diagram-min.js"></script>
<script>
// render the codes of type diagram to actual diagrams ^^
$(function(){
    $(".diagram code").each(function(){
        var txt = $(this).text();
        $('<div class="render-diagram"/>').text(txt).insertAfter($(this).parent());
    });
    $(".render-diagram").sequenceDiagram({theme: 'simple'});
});
</script>