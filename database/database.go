// Copyright 2017 BlueMasters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This package handles persistent storage for the TTUDP. It uses BoltDB.

package database // import "gitlab.com/telecom-tower/client-ttudp/database"

//go:generate /bin/bash -c "protoc --go_out=. *.proto"

import (
	"os"
	"path"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	dbName            = "ttudp.bolt"
	topScoreBucket    = "TopScore"
	dailyScoreBucket  = "DailyScore"
	hourlyScoreBucket = "HourlyScore"
	positionsBucket   = "positions"
)

// TTDB represent the database used for TTUDP
// todo: replace TTDB by just bolt.DB
type TTDB struct {
	*bolt.DB
}

// NewttDB opens the TTUDP database or creates it if required.
// The path of the database file is env[SNAP_DATA] if defined,
// otherwise the path is the current directory.
func NewttDB() (*TTDB, error) {
	var err error
	baseDir := os.Getenv("SNAP_DATA")
	if baseDir == "" {
		baseDir, err = os.Getwd()
		if err != nil {
			baseDir = "."
		}
	}
	filename := path.Join(baseDir, dbName)
	res := new(TTDB)
	res.DB, err = bolt.Open(filename, 0600, nil)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// ProtoPut writes a proto Message to a bolt bucket.
func ProtoPut(b *bolt.Bucket, key []byte, v proto.Message) error {
	data, err := proto.Marshal(v)
	if err != nil {
		log.Error(errors.WithMessage(err, "Error marshalling JSON"))
		return err
	}
	err = b.Put(key, data)
	if err != nil {
		log.Error(errors.WithMessage(err, "Error writing key"))
	}
	return err
}

// Init creates the buckets in BoltDB to store scores
func (db *TTDB) Init() error {
	err := db.Update(func(tx *bolt.Tx) error {
		for _, name := range []string{topScoreBucket, dailyScoreBucket, hourlyScoreBucket, positionsBucket} {
			if _, err := tx.CreateBucketIfNotExists([]byte(name)); err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		log.Error(errors.WithMessage(err, "Error updating DB"))
		return err
	}
	// load bookings to memory
	err = db.InitBookings()
	if err != nil {
		log.Error(errors.WithMessage(err, "Error initializing booking"))
	}
	return err
}
