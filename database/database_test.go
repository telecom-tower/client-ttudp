package database

import (
	"fmt"
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
)

func DumpDatabase(db *TTDB) { // nolint: megacheck
	dumpScoreBucket := func(bucketName string) {
		fmt.Printf("----- %v -----\n", bucketName)
		err := db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte(bucketName))
			if b == nil {
				return fmt.Errorf("Unable to use bucket %v", bucketName)
			}
			c := b.Cursor()
			for k, v := c.First(); k != nil; k, v = c.Next() {
				record := new(Score)
				if err := proto.Unmarshal(v, record); err != nil {
					return err
				}
				fmt.Printf("%v -> %v\n", string(k), record.String())
			}
			return nil
		})
		if err != nil {
			fmt.Println(err)
		}
	}

	dumpScoreBucket(topScoreBucket)
	dumpScoreBucket(dailyScoreBucket)
	dumpScoreBucket(hourlyScoreBucket)
}

func TestDatabase(t *testing.T) {
	os.Remove("ttudp.bolt")
	db, err := NewttDB()
	assert.NoError(t, db.Init())
	assert.NoError(t, err)
	defer db.Close()
}
