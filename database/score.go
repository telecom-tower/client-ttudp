package database

import (
	"bytes"
	"fmt"
	"math"
	"os"
	"sort"
	"time"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	topX = 10 // number of record to keep for each prefix
)

// We define byScoreEntry and byScore to implement score sorting (with the highest score first)
type byScoreEntry struct {
	key   []byte
	score *Score
}

type byScore []byScoreEntry

func (s byScore) Len() int {
	return len(s)
}

func (s byScore) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s byScore) Less(i, j int) bool {
	// Watch the >= sign below! A higher score means a lower position.
	return s[i].score.GetScore() >= s[j].score.GetScore()
}

// getAllScores is a helper function to retrieve all scores from a bucket. It is used by TopScores, TopDailyScores,
// TopHourlyScores, and also by SaveScores.
func getAllScores(b *bolt.Bucket, prefix []byte) (byScore, error) {
	list := make(byScore, 0)
	addScore := func(k, v []byte) error {
		ts := new(Score)
		if err := proto.Unmarshal(v, ts); err != nil {
			return err
		}
		list = append(list, byScoreEntry{key: k, score: ts})
		return nil
	}

	c := b.Cursor()
	if len(prefix) == 0 {
		for k, v := c.First(); k != nil; k, v = c.Next() {
			if err := addScore(k, v); err != nil {
				return nil, err
			}
		}
	} else {
		for k, v := c.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = c.Next() {
			if err := addScore(k, v); err != nil {
				return nil, err
			}
		}
	}
	return list, nil
}

// timeNow gives the current time. For testing purposes, you can set the SIMULATED_TIME environment variable to a valid
// ANSI-C timestamp and we will use this instead of the real current time. Note that this is for testing purposes
// only and should not be used in production.
func timeNow() time.Time {
	now := time.Now()
	if s := os.Getenv("SIMULATED_TIME"); s != "" {
		if n, err := time.Parse(time.ANSIC, s); err == nil {
			now = n
		}
	}
	return now
}

// sorted get an unsorted list of ScoreEntries and returns a sorted, bounded list of Scores
func sorted(list byScore) []*Score {
	sort.Sort(list)
	// cut the list if more than topX entries
	if len(list) > topX {
		list = list[0:topX]
	}
	var result []*Score
	for _, v := range list {
		result = append(result, v.score)
	}
	return result
}

// TopScores returns the top scores of all the times
func (db *TTDB) TopScores() ([]*Score, error) {
	var list byScore
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topScoreBucket))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", topScoreBucket)
		}
		var err error
		if list, err = getAllScores(b, []byte{}); err != nil {
			return err
		}
		log.Debugf("Retrieved %v scores from TopScores", list.Len())
		return nil
	})
	if err != nil {
		return nil, err
	}
	return sorted(list), nil
}

// TopDailyScores returns the top daily scores.
func (db *TTDB) TopDailyScores(day time.Time) ([]*Score, error) {
	var list byScore
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(dailyScoreBucket))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", dailyScoreBucket)
		}
		prefix := []byte(fmt.Sprintf("%016X:", day.Truncate(24*time.Hour).Unix()))
		var err error
		if list, err = getAllScores(b, prefix); err != nil {
			return err
		}
		log.Debugf("Retrieved %v scores from TopDailyScores", list.Len())
		return nil
	})
	if err != nil {
		return nil, err
	}
	return sorted(list), nil
}

// TopHourlyScores returns the top scores made in the last 60 minutes.
func (db *TTDB) TopHourlyScores() ([]*Score, error) {
	now := timeNow()
	var list byScore
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(hourlyScoreBucket))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", hourlyScoreBucket)
		}
		// delete outdated scores
		c := b.Cursor()
		maxKey, _ := c.Seek([]byte(fmt.Sprintf("%016X", now.Add(-1*time.Hour).Unix())))
		if maxKey != nil {
			for k, _ := c.First(); k != nil && !bytes.Equal(k, maxKey); k, _ = c.Next() {
				err := b.Delete(k)
				if err != nil {
					log.Error(errors.WithMessage(err, "Error deleting score"))
					return err
				}
			}
		} else { // delete all
			for k, _ := c.First(); k != nil; k, _ = c.Next() {
				err := b.Delete(k)
				if err != nil {
					log.Error(errors.WithMessage(err, "Error deleting all score"))
					return err
				}
			}
		}
		// get remaining scores
		var err error
		if list, err = getAllScores(b, []byte{}); err != nil {
			return err
		}
		log.Debugf("Retrieved %v scores from TopHourlyScores", list.Len())
		return nil
	})
	if err != nil {
		return nil, err
	}
	return sorted(list), nil

}

// CleanupDailyScores removes outdated scores from the saily scores. The argument maxDay is the first day
// not deleted by this method.
func (db *TTDB) CleanupDailyScores(maxDay time.Time) error {
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(dailyScoreBucket))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", dailyScoreBucket)
		}
		c := b.Cursor()
		maxKey, _ := c.Seek([]byte(fmt.Sprintf("%016X", maxDay.Truncate(24*time.Hour).Unix())))
		if maxKey != nil {
			for k, _ := c.First(); k != nil && !bytes.Equal(k, maxKey); k, _ = c.Next() {
				err := b.Delete(k)
				if err != nil {
					log.Error(errors.WithMessage(err, "Error deleting daily score"))
					return err
				}
			}
		}
		return nil
	})
	return err
}

// SaveScore adds a new score to the database
func (db *TTDB) SaveScore(username string, score uint32) error { // nolint: gocyclo
	now := timeNow()
	log.Debugf("Saving score of %s (%v)", username, score)
	// saveInBucket is an internal "generic" function to save the 3 type of scores
	saveInBucket := func(tx *bolt.Tx, bucketName string, searchPrefix, insertPrefix []byte, maxLen int) error {
		b := tx.Bucket([]byte(bucketName))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", bucketName)
		}
		insertKey := append(insertPrefix, []byte(username)...)
		list, err := getAllScores(b, searchPrefix)
		if err != nil {
			return err
		}
		for _, ts := range list {
			if bytes.Equal(ts.key, insertKey) {
				log.Debugf("%s is already in the top scores of %s", username, bucketName)
				if score > ts.score.GetScore() {
					log.Debugf("Score is higher (%v > %v), updating", score, ts.score.GetScore())
					ts.score.Score = proto.Uint32(score)
					ts.score.Timestamp = proto.Int64(now.Unix())
					return ProtoPut(b, insertKey, ts.score)
				}
				log.Debugf("Score is not higher (%v <= %v), ignoring", score, ts.score.GetScore())
				return nil
			}
		}
		log.Debugf("%v is not yet in the top scores of %s", username, bucketName)
		newScoreEntry := byScoreEntry{
			score: &Score{
				Username:  proto.String(username),
				Score:     proto.Uint32(score),
				Timestamp: proto.Int64(now.Unix()),
			},
			key: insertKey,
		}

		list = append(list, newScoreEntry)
		sort.Sort(list)

		for i, v := range list {
			if i < maxLen && v.score.GetUsername() == username {
				// The user is in the topX (maxLen), so save it
				log.Debugf("Adding %s in the top score of %s", username, bucketName)
				if err := ProtoPut(b, newScoreEntry.key, newScoreEntry.score); err != nil {
					return err
				}
			} else if i >= maxLen && v.score.GetUsername() != username {
				log.Debugf("%s is no longer in the top score of %s. Deleting", v.score.GetUsername(), bucketName)
				if err := b.Delete(v.key); err != nil {
					return err
				}
			}
		}

		return nil
	}

	err := db.Update(func(tx *bolt.Tx) error {
		// Save topScore
		log.Debug("Updating topScoreBucket")
		searchPrefix := []byte{}
		insertPrefix := searchPrefix
		if err := saveInBucket(tx, topScoreBucket, searchPrefix, insertPrefix, topX); err != nil {
			log.Error(errors.WithMessage(err, "Error saving in bucket"))
			return err
		}

		// Save dailyScore
		log.Debug("Updating dailyScoreBucket")
		day := now.Truncate(24 * time.Hour)
		searchPrefix = []byte(fmt.Sprintf("%016X:", day.Unix()))
		insertPrefix = searchPrefix
		if err := saveInBucket(tx, dailyScoreBucket, searchPrefix, insertPrefix, topX); err != nil {
			log.Error(errors.WithMessage(err, "Error saving in bucket"))
			return err
		}

		// cleanupHourly
		log.Debug("Updating hourlyScoreBucket")
		b := tx.Bucket([]byte(hourlyScoreBucket))
		if b == nil {
			err := errors.Errorf("Unable to use bucket %v", hourlyScoreBucket)
			log.Error(err)
			return err
		}
		c := b.Cursor()
		maxKey, _ := c.Seek([]byte(fmt.Sprintf("%016X", now.Add(-1*time.Hour).Unix())))
		if maxKey != nil {
			for k, _ := c.First(); k != nil && !bytes.Equal(k, maxKey); k, _ = c.Next() {
				err := b.Delete(k)
				if err != nil {
					log.Error(errors.WithMessage(err, "Error deleting key"))
					return err
				}
			}
		} else { // delete all
			for k, _ := c.First(); k != nil; k, _ = c.Next() {
				err := b.Delete(k)
				if err != nil {
					log.Error(errors.WithMessage(err, "Error deleting key"))
					return err
				}
			}
		}

		// Save hourlyScore
		searchPrefix = []byte{}
		insertPrefix = []byte(fmt.Sprintf("%016X:", now.Unix()))
		// This is tricky: for the hourly scores, we need to store all entries. If an entry is not yet in
		// the topX, it might get there when the best scores expire. It would be perhaps nicer to have
		// a special value (such as -1) to indicate "unlimited", but MaxInt32 is large (2 Billions) and
		// it will be a problem in we have to store 2 billions records in 1 hour, this means 596'523 records
		// per second! So this is safe enough for our case.
		return saveInBucket(tx, hourlyScoreBucket, searchPrefix, insertPrefix, math.MaxInt32)
	})
	return err
}
