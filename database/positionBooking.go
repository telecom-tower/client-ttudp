package database

import (
	"encoding/base64"
	"fmt"
	"math/rand"
	"time"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
)

// BookedPositions is a map from an ID to a position
var BookedPositions map[int32]*PositionBooking

// IsBooked checks if a position is already booked.
func IsBooked(position int32) bool {
	booking, exists := BookedPositions[position]
	if exists && booking.IsExpired() {
		// cleanup
		delete(BookedPositions, position)
		exists = false
	}
	return exists
}

// IsBookingValid checks if a given booking is valid
func (db *TTDB) IsBookingValid(position int32, bookingKey string) bool {
	booking, exists := BookedPositions[position]
	if exists && booking.IsExpired() {
		// cleanup
		db.DeleteBooking(position) // nolint: errcheck
		exists = false
	}
	return exists && *booking.Key == bookingKey
}

// NewPositionValue creates a new PositionBooking
func NewPositionValue(pos int, hours int) *PositionBooking {
	var p PositionBooking
	p32 := int32(pos)
	p.Position = &p32
	p.SetExpire(hours)
	p.NewKey()
	return &p
}

// NiceString formats a booking into a string
func (pv *PositionBooking) NiceString() string {
	return fmt.Sprintf("Key=%s, Position=%d, Expire=%v", *(pv.Key), *(pv.Position), time.Unix(0, *(pv.Expire)))
}

// IsExpired checks if a booking expired
func (pv *PositionBooking) IsExpired() bool {
	now := time.Now().UnixNano()
	return *(pv.Expire) < now
}

// SetExpire sets the expiration of a booking
func (pv *PositionBooking) SetExpire(hours int) {
	expire := time.Now().Add(time.Duration(hours) * time.Hour).UnixNano()
	pv.Expire = &expire
}

// NewKey generates a new randon key
func (pv *PositionBooking) NewKey() {
	token := make([]byte, 4)
	rand.Read(token) // nolint: gas
	key := base64.StdEncoding.EncodeToString(token)
	pv.Key = &key
}

// InitBookings initializes the booking DB
func (db *TTDB) InitBookings() error {
	BookedPositions = make(map[int32]*PositionBooking)

	err := db.View(func(tx *bolt.Tx) error {
		// get bucket
		bucket := tx.Bucket([]byte(positionsBucket))
		err := bucket.ForEach(func(k, v []byte) error {
			// unmarshal record
			booking := new(PositionBooking)
			if err := proto.Unmarshal(v, booking); err != nil {
				return err
			}
			if booking.IsExpired() {
				// delete expired record
				err := bucket.Delete(k)
				if err != nil {
					return err
				}
			} else {
				// add valid record to cache
				BookedPositions[*(booking.Position)] = booking
			}
			return nil
		})
		return err
	})
	return err
}

// DeleteBooking removes a booking form the DB
func (db *TTDB) DeleteBooking(position int32) error {
	return db.Update(func(tx *bolt.Tx) error {
		// get bucket
		b := tx.Bucket([]byte(positionsBucket))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", positionsBucket)
		}
		// get key
		positionStr := fmt.Sprintf("%d", position)
		// remove record from db
		if err := b.Delete([]byte(positionStr)); err != nil {
			return err
		}
		// delete from cache
		delete(BookedPositions, position)
		return nil
	})
}

// AddBooking adds a booking to the DB
func (db *TTDB) AddBooking(pb *PositionBooking) error {
	return db.Update(func(tx *bolt.Tx) error {
		// get bucket
		b := tx.Bucket([]byte(positionsBucket))
		if b == nil {
			return fmt.Errorf("Unable to use bucket %v", positionsBucket)
		}
		positionStr := fmt.Sprintf("%d", *(pb.Position))
		// save it to db
		if err := ProtoPut(b, []byte(positionStr), pb); err != nil {
			return err
		}
		// save it to cache
		BookedPositions[*(pb.Position)] = pb
		return nil
	})
}
