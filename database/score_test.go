package database

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTopScore(t *testing.T) {
	os.Remove("ttudp.bolt")
	db, err := NewttDB()
	assert.NoError(t, err)
	defer db.Close()
	assert.NoError(t, db.Init())

	simnow := time.Date(2017, 1, 2, 14, 0, 0, 0, time.Local)
	fmt.Printf("Setting time to %v\n", simnow.Format(time.ANSIC))
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err := db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, 0, len(list))

	assert.NoError(t, db.SaveScore("Lucy", 1000))
	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, 1, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Damien", 800))
	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())

	for i := 0; i < topX-2; i++ {
		simnow = simnow.Add(5 * time.Minute)
		os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
		assert.NoError(t, db.SaveScore(fmt.Sprintf("User%03d", i), 600+uint32(i*10)))
	}

	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Looser", 10))
	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Jacques", 900))
	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Jacques", list[1].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Damien", 2000))
	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Damien", list[0].GetUsername())
	assert.Equal(t, "Lucy", list[1].GetUsername())
	assert.Equal(t, "Jacques", list[2].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Winner", 3000))
	list, err = db.TopScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Winner", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())
	assert.Equal(t, "Lucy", list[2].GetUsername())
	assert.Equal(t, "Jacques", list[3].GetUsername())

	// DumpDatabase(db)
}

func TestDailyScore(t *testing.T) {
	os.Remove("ttudp.bolt")
	db, err := NewttDB()
	assert.NoError(t, err)
	defer db.Close()
	assert.NoError(t, db.Init())

	simnow := time.Date(2017, 1, 2, 14, 30, 10, 0, time.Local)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))

	list, err := db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 0, len(list))

	assert.NoError(t, db.SaveScore("Lucy", 1000))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Damien", 800))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())

	for i := 0; i < topX-2; i++ {
		simnow = simnow.Add(5 * time.Minute)
		os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
		assert.NoError(t, db.SaveScore(fmt.Sprintf("User%03d", i), 600+uint32(i*10)))
	}

	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Looser", 10))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Jacques", 900))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, "Jacques", list[1].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Damien", 2000))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Damien", list[0].GetUsername())
	assert.Equal(t, "Lucy", list[1].GetUsername())
	assert.Equal(t, "Jacques", list[2].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Winner", 3000))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Winner", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())
	assert.Equal(t, "Lucy", list[2].GetUsername())
	assert.Equal(t, "Jacques", list[3].GetUsername())

	// Tomorrow...
	simnow = time.Date(2017, 1, 3, 10, 30, 10, 0, time.Local)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 0, len(list))

	assert.NoError(t, db.SaveScore("Lucy", 100))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())

	// Check the day before
	list, err = db.TopDailyScores(simnow.Add(-24 * time.Hour))
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "Winner", list[0].GetUsername())
	assert.Equal(t, "Damien", list[1].GetUsername())
	assert.Equal(t, "Lucy", list[2].GetUsername())
	assert.Equal(t, "Jacques", list[3].GetUsername())

	simnow = simnow.Add(5 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Damien", 800))
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.Equal(t, "Damien", list[0].GetUsername())
	assert.Equal(t, "Lucy", list[1].GetUsername())

	// Add an old record
	old := time.Date(2016, 10, 4, 30, 12, 4, 0, time.Local)
	os.Setenv("SIMULATED_TIME", old.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("OldMan", 1200))

	// Cleanup
	assert.NoError(t, db.CleanupDailyScores(simnow))

	// Check the day before
	list, err = db.TopDailyScores(simnow.Add(-24 * time.Hour))
	assert.NoError(t, err)
	assert.Equal(t, 0, len(list))

	// recheck now
	list, err = db.TopDailyScores(simnow)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.Equal(t, "Damien", list[0].GetUsername())
	assert.Equal(t, "Lucy", list[1].GetUsername())

	// DumpDatabase(db)
}

func TestHourlyScore(t *testing.T) {
	os.Remove("ttudp.bolt")
	db, err := NewttDB()
	assert.NoError(t, err)
	defer db.Close()
	assert.NoError(t, db.Init())

	simnow := time.Date(2015, 4, 2, 14, 30, 10, 0, time.Local)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))

	list, err := db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 0, len(list))

	assert.NoError(t, db.SaveScore("Lucy", 2000))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 1, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())

	// 10 minutes later, Lucy again
	simnow = simnow.Add(10 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Lucy", 1000))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, 2000, int(list[0].GetScore()))
	assert.Equal(t, "Lucy", list[1].GetUsername())
	assert.Equal(t, 1000, int(list[1].GetScore()))

	// 10 minutes later, Damien
	simnow = simnow.Add(10 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	assert.NoError(t, db.SaveScore("Damien", 1500))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 3, len(list))
	assert.Equal(t, "Lucy", list[0].GetUsername())
	assert.Equal(t, 2000, int(list[0].GetScore()))
	assert.Equal(t, "Damien", list[1].GetUsername())
	assert.Equal(t, 1500, int(list[1].GetScore()))
	assert.Equal(t, "Lucy", list[2].GetUsername())
	assert.Equal(t, 1000, int(list[2].GetScore()))

	// 45 minutes later, Lucy's first score should disappear
	simnow = simnow.Add(45 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.Equal(t, "Damien", list[0].GetUsername())
	assert.Equal(t, 1500, int(list[0].GetScore()))
	assert.Equal(t, "Lucy", list[1].GetUsername())
	assert.Equal(t, 1000, int(list[1].GetScore()))

	// 10 minutes later, Damien should be alone
	simnow = simnow.Add(10 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 1, len(list))
	assert.Equal(t, "Damien", list[0].GetUsername())
	assert.Equal(t, 1500, int(list[0].GetScore()))

	// 10 minutes later, the list should be empty
	simnow = simnow.Add(10 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 0, len(list))

	// save 90 entries with 1 minute interval
	for i := 0; i < 90; i++ {
		simnow = simnow.Add(1 * time.Minute)
		os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
		assert.NoError(t, db.SaveScore(fmt.Sprintf("User%03d", i), uint32(600-i)))
	}
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "User029", list[0].GetUsername())

	// 12 minutes later...
	simnow = simnow.Add(12 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, topX, len(list))
	assert.Equal(t, "User041", list[0].GetUsername())

	// 45 minutes later...
	simnow = simnow.Add(45 * time.Minute)
	os.Setenv("SIMULATED_TIME", simnow.Format(time.ANSIC))
	list, err = db.TopHourlyScores()
	assert.NoError(t, err)
	assert.Equal(t, 4, len(list))
	assert.Equal(t, "User086", list[0].GetUsername())

	//DumpDatabase(db)

}
